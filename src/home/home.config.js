angular.module('home.config', [])
.config(function($locationProvider, $urlRouterProvider, $stateProvider) {
  
  $stateProvider
  .state('app.home', {
    url: '/home',
    templateUrl: 'home/views/home.tpl.html',
    controller: 'home.controller',
    resolve:{
      escapemapData: function(escapemap){
        return escapemap.getMap().then(function(d){
          return d.data;
        });
      }
    }
    });
});