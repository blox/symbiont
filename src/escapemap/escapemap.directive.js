angular.module('escapemap.directive', [])
.directive('escapemap', function($window, $timeout, $location) {
  return {
    restrict: 'AE',
    scope:{
      data:'='
    },
    link: function(scope, ele, attrs) {
      
      var renderTimeout;
      var w = scope.data.map.width;
      var h = scope.data.map.height;
      
      var svg = d3.select(ele[0])
        .append('svg')
        .attr('width', w)
        .attr('height', h);

      var lineFunction = d3.svg.line()
        .x(function(d) { return d.x; })
        .y(function(d) { return d.y; })
        .interpolate("linear");
       
      scope.$watch('data', function(newData){
        w = newData.map.width;
        h = newData.map.height;
        svg.attr('height',h);
        svg.attr('width',w);
        scope.render(newData);
      });
      
      
      scope.render = function(data){
        
        svg.selectAll('*').remove();
        
        if(!data){
          //return;
        }
        
        if(renderTimeout){
          $timeout.cancel(renderTimeout);
        }

        renderTimeout = $timeout(function(){
          
          /* DRAW WALLS */
          svg
            .selectAll(".wall")
            .data(data.walls)
            .enter()
            .append("path")
            .attr("d", function(d){
              return lineFunction(d);
            })
            .style("stroke", "black")
            .attr("stroke-width", data.map.wallWidth)
            .attr("fill", "none");
          
          
          /* DRAW CREW */          
          svg
            .selectAll(".crew")
            .data(data.crew)
            .enter()
            .append("circle")
            .attr("cy", function(d,i){
              d.y = (d.r)+(data.crew[i-1]?data.crew[i-1].y:0)+((i==0)?0:data.crew[i-1].r)+((i>0)?data.map.crewOffset:0);
              return d.y;
            })
            .attr("cx", function(d){
              d.x = d.r;
              return (d.x);
            })
            .attr("r", function(d){
              return d.r;
            })
            .attr("fill", function(d) {
              return d.c;
            });
          
          /* DRAW SHIPS */
          svg
            .selectAll(".ship")
            .data(data.ships)
            .enter()
            .append("circle")
            .attr("cy", function(d,i){
              d.y = (d.r)+(data.ships[i-1]?data.ships[i-1].y:0)+((i==0)?0:data.ships[i-1].r)+((i>0)?data.map.shipOffset:0);;
              return d.y;
            })
            .attr("cx", function(d){
              d.x = w-(d.r);
              return d.x;
            })
            .attr("r", function(d){
              return d.r;
            })
            .attr("fill", function(d) {
              return d.c;
            });
            
            
          /*   Where are all the doors?
           *
           *   METHOD DESCRIPTION:
           *   x-coordinate hashmap that contains y-intercepts of gaps between walls "aka" doors.
           *
           *   NOTE:
           *   In this algorithm I'm assuming each `data.walls[i]` contains a set of paths where each `data.walls[i][j].x` is the same.
           *   The second a path has a variant `data.walls[i][j].x != data.walls[i][j+1].x` or "curve" this algorithm falls apart.
           *   I'm also assuming that for each subset of `data.walls[i][j].x` there is a ``data.walls[i][j].y` that starts at 0.
           */
          
          var doors = [];
          
          for(var i = 0;i<data.walls.length;i++){
            
            var b1 = d3.max(data.walls[i], function(d) { return d.y; });
            var b2 = data.walls[i+1]&&(data.walls[i][0].x==data.walls[i+1][0].x)?d3.min(data.walls[i+1], function(d) { return d.y; }):h;
                   
            if(b1!=b2){
              doors.push({
                b1:b1+data.map.doorOffset,
                b2:b2-data.map.doorOffset,
                x1:data.walls[i][0].x-(data.map.wallWidth/2),
                x2:data.walls[i][0].x+(data.map.wallWidth/2),
                x:data.walls[i][0].x,
                y:b1+((b2-b1)/2),
                gap:(b2-b1)-(data.map.doorOffset*2),
                capacity:0,
                offset:0
              });
            }
          }
          
          
          /*    We need an array of x-plane entities
           *
           *    METHOD DESCRIPTION:
           *    Sorts x-planes to facilitate graph relationships
           *
           *    NOTE:
           *    It is being assumed that every `data.crew[i]` and `data.ships[i]` exist on the same x-planes.
           */
          
          var xPlanes = {};
          var xPlanesArray = [];
          
          function xPlaneMapper(obj){
            for(var i=0;i<obj.length;i++){
              if(!xPlanes[obj[i].x]){
                xPlanes[obj[i].x] = [];
              }
              xPlanes[obj[i].x].push(obj[i]);
            }            
          }
          
          xPlaneMapper(doors);
          
          var sortArrays = [];
          for(var i in xPlanes){
            sortArrays.push(i);
          }
          sortArrays = sortArrays.sort(function (a, b) { 
              return a - b;
          });
          for(var i = 0;i<sortArrays.length;i++){
            xPlanesArray.push(xPlanes[sortArrays[i]]);
          }

          xPlanesArray.splice(0,0,data.crew);
          xPlanesArray.splice(xPlanesArray.length,0,data.ships);
          
          
          /*    We need to build a map of graph relationships
           *
           *    METHOD DESCRIPTION:
           *    Builds a map of graph relationships
           *
           *    NOTE:
           *    What's interesting about this graph is that nodes in
           *    the begining and end only connect with nodes in the middle.
           *    
           */
          
          var map = {};
          var nodes = {};
          
          function hash(obj){
            return CryptoJS.MD5(obj).toString(CryptoJS.enc.Hex);
          }
          
          function pythagorean(x1,x2,y1,y2){
            var a = (x1-x2);
            var b = (y1-y2);
            return Math.sqrt((a * a) + (b * b));
          }
          
          function generateChildren(map,obj){
            for(var i = 0;i<obj.length;i++){
              for(var j = 0;j<obj[i].length;j++){
                var parent = hash(obj[i][j].x.toString()+obj[i][j].y.toString());
                map[parent] = {};
                nodes[parent] = {node:obj[i][j],children:{}};
                obj[i][j].id = parent;
                if(obj[i+1]){
                  for(var q = 0;q<obj[i+1].length;q++){
                    var child = hash(obj[i+1][q].x.toString()+obj[i+1][q].y.toString());
                    map[parent][child] = pythagorean(obj[i+1][q].x,obj[i][j].x,obj[i+1][q].y,obj[i][j].y);
                    nodes[parent].children[child] = {node:obj[i+1][q]};
                  }
                }
              }
            }
          }
          
          generateChildren(map,xPlanesArray);
          
          
          /*    We need to find the initial shortest paths for each node
           *
           *    METHOD DESCRIPTION:
           *    Generate shortest paths for nodes using Dijkstra's algorithm
           *
           *    NOTE:
           *    What's interesting about this graph is that nodes in
           *    the begining and end only connect with nodes in the middle.
           *    
           */          
          
          
          var fNodes = xPlanesArray[0];
          var lNodes = xPlanesArray[xPlanesArray.length-1];

          function findPaths(map){
            var graph = new Graph(map);
            var paths = [];
            for(var i = 0;i<fNodes.length;i++){
              for(var j = 0;j<lNodes.length;j++){
                if(fNodes[i].t == lNodes[j].t){
                  paths.push(graph.findShortestPath(fNodes[i].id,lNodes[j].id));
                }
              }
            }
            return paths;
          }
          
          var paths = findPaths(map);
          
          
          /*    We need to adjust/remove paths that wont fit through the doors!
           *
           *    METHOD DESCRIPTION:
           *    Iterate through each `fNodes[i]` and adjust any paths based on gaps of `mNodes[i]`
           *
           *    NOTE:
           *    Were defining gap sizes in pixels. The path thickness is a `fNodes[i].r * 2`
           *    
           */              
          
          var mArrays = xPlanesArray.slice(1,xPlanesArray.length-1);
          var mNodes = {};
          
          for(var i = 0;i<mArrays.length;i++){
            for(var j = 0;j<mArrays[i].length;j++){
              mNodes[mArrays[i][j].id]=mArrays[i][j];
            }
          }
          
          function tryAgain(i,j,node,lineThickness){
            if((mNodes[node].capacity+lineThickness+(mNodes[node].capacity==0?0:data.map.pathOffset))<=mNodes[node].gap){
              mNodes[node].capacity = mNodes[node].capacity + lineThickness + (mNodes[node].capacity==0?0:data.map.pathOffset);
            }
            else{
              delete map[paths[i][j-1]][node];
              var graph = new Graph(map);
              var newPath = graph.findShortestPath(paths[i][0],paths[i][paths[i].length-1]);
              paths[i] = newPath?newPath:[];
              if(paths[i].length>0){
                var newNode = paths[i][j];
                tryAgain(i,j,newNode,lineThickness);
              }
            }  
          }
          
          for(var i = 0;i<paths.length;i++){
            for(var j = 0;j<paths[i].length;j++){
              var node = paths[i][j];
              var lineThickness = nodes[paths[i][0]].node.s;
              if(mNodes[node]){
                tryAgain(i,j,node,lineThickness);
              }
            }
          }
          
          /*    We need to generate the coordinates for each path
           *
           *    METHOD DESCRIPTION:
           *    Iterate through each `paths[i]` and generate coordinates.
           *
           *    NOTE:
           *    At any middle node, there are two coordinates that need to be drawn to account
           *    for the thickness of the wall. There also needs to be a measure of y-offset as
           *    paths stack inside the space of the door.
           *    
           */             
          
          var coords = [];
          
          for(var i = 0;i<paths.length;i++){
            var path = [];
            for(var j = 0;j<paths[i].length;j++){
              var lineThickness = nodes[paths[i][0]].node.s;
              var lineColor = nodes[paths[i][0]].node.c;
              var node = paths[i][j];
              if(mNodes[node]){
                var pathOffset = (nodes[node].node.offset==0?0:data.map.pathOffset);
                var totalOffset = nodes[node].node.offset + (lineThickness) + pathOffset;
                
                var obj1 = {
                  x:nodes[node].node.x1,
                  y:nodes[node].node.b1+nodes[node].node.offset + pathOffset + (lineThickness/2)
                }
                var obj2 = {
                  x:nodes[node].node.x2,
                  y:nodes[node].node.b1+nodes[node].node.offset + pathOffset + (lineThickness/2)
                }
                
                path.push(obj1);
                path.push(obj2);
                
                nodes[node].node.offset = totalOffset;
              }
              else{
                var obj = {
                  x:nodes[node].node.x,
                  y:nodes[node].node.y,
                  r:lineThickness,
                  c:lineColor
                }
                path.push(obj);
              }
            }
            coords.push(path);
          }
          

          /* DRAW PATHS EACH CREW MEMBER MUST TAKE */
          svg
            .selectAll(".path")
            .data(coords)
            .enter()
            .append("path")
            .attr("d", function(d){
              return lineFunction(d);
            })
            .style("stroke", function(d){
              return d[0]?d[0].c:0;
            })
            .attr("stroke-width", function(d){
              return d[0]?d[0].r:0;
            })
            .attr("fill", "none");
          
        },200);
      }
    }
  };
});