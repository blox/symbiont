angular.module('escapemap.service', [])

.factory('escapemap', function($q, $http) {
  var _escapemap = {};

  _escapemap.getMap = function(){
    return $http.get('/json/escapemap.json');
  }
  
  return _escapemap;
});