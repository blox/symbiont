angular.element(document).ready(function() {

  var browser = {
    match : false,
    supported : false 
  };
  
  var browsers = {
    "Firefox":{
      name:"Firefox",
      version:34,
      offset:3,
      link:"https://www.mozilla.org/en-US/firefox/new"
    },
    "Chrome":{
      name:"Chrome",
      version:39,
      offset:3,
      link:"https://www.google.com/chrome/"
    },
    "CriOS":{
      name:"CriOS",
      version:39,
      offset:3,
      link:"https://www.google.com/chrome/"
    }
  };
  
  for (var i in browsers) {
    var s = navigator.userAgent;
    var x = s.indexOf(browsers[i].name);
    var l = (browsers[i].name.length + 1);
    if(x !== -1 && !browser.match){
      browser.match = true;
      browser.supported = true;
      browser.name = browsers[i].name;
      browser.currentVersion =  parseInt(s.substring(x+l,x+l+browsers[i].offset));
      browser.supportedVersion = (parseInt(s.substring(x+l,x+l+browsers[i].offset)) >= browsers[i].version)?true:false;
    }
  }

  function addDiv(classes, selector, innerHTML){
    var div = document.createElement('div');
    div.className += classes;
    div.innerHTML += innerHTML?innerHTML:'';
    document.querySelector(selector).appendChild(div); 
  }

  function buildMessage(message){
    addDiv('support', 'body');
    addDiv('bg', 'body > .support');
    addDiv('overlay', 'body > .support');
    addDiv('popup', 'body > .support');
    addDiv('logo', 'body > .support > .popup', 'Hey,');
    addDiv('text', 'body > .support > .popup', message);
  }
  
  //We do not support this browser.
  if(!browser.supported){
    var message = '';
    var list = '';
    message +=  'This <b>browser</b> is not supported.<br/><br/>' +
                'We currently support the following:<br/><br/>';
    for (var i in browsers) {
      list += '<a href="' + browsers[i].link + '" target="_blank">' +
              browsers[i].name +
              '</a><br/>'
    }
    message += list + '<br/>';
    message += 'Please install one of these to continue.'
    buildMessage(message);
  }

  //We support this browser but not it's current version.
  if(browser.supported && !browser.supportedVersion){
    var message = '';
    var list = '';
    message +=  '<b>' + browser.name + '</b> is a supported browser but your current version ( <b>'+ browser.currentVersion + '</b> ) is not.<br/><br/>' +
                'Please update to continue:<br/><br/>';
    list += '<a href="' + browsers[browser.name].link + '" target="_blank">' +
            'Update ' +
            browser.name +
            '</a><br/>'
    message += list;
    buildMessage(message);
  }
  
  //We support this browser and it's current version.
  if(browser.supported && browser.supportedVersion){
    angular.bootstrap(document, ['app']);
  }
});