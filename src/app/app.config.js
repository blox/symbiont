angular.module('app.config', [])

.config(function($locationProvider, $urlRouterProvider, $stateProvider) {
  
  $urlRouterProvider.otherwise('/home');
  $locationProvider.html5Mode(true);
  
  $stateProvider
  .state('app', {
    abstract: true,
    templateUrl: 'app/views/app.tpl.html',
    controller: 'app.controller'
  });
});