metamorph
=====================

## Getting started

```bash
$ npm install -g gulp bower
$ npm install
$ bower install
```

## Builds

```bash
$ gulp build
$ gulp watch
```

## Thought process

#### First thoughts

Wow, this is not your average code test. This is going to take a lot more muscle power than just some native JavaScript.

I'm sure after a few days of writing native SVG boiler plate I could definitely get something similar off the ground - but there are tools available to us people spent their entire PhD writing.

The particular tool I had in mind for this project was **[D3](http://d3js.org/).**

*"D3.js (or just D3 for Data-Driven Documents) is a JavaScript library for producing dynamic, interactive data visualizations in web browsers. It makes use of the widely implemented SVG, HTML5, and CSS standards."*

It is my opinion the most powerful visualization library for generating graphics under mathematical constraints available to us as developers. Nothing else comes close.

#### Boiler plate

First things first, let's whip up a new Gitlab repo and copy paste some boilerplate from a recent project so I can get going fast.

AngularJS, Gulp, Bower, Node.

Let's also make a directive skeleton based off some other D3 directives I've made in the past.

Now - if these things weren't already at my finger tips - configuring and setting up a sophisticated build with AngularJS and Gulp would probably take me several days.

However, since I have them available to me, it **dramatically** increases my rate of development, so I'm going to use them.

#### All the pieces

I spent a couple hours just staring at **[this picture](https://github.com/metamorph-inc/Escape-map/blob/master/escape-map-example.png)** trying to understand the best way to break it up.

Some random thoughts I wrote down as I went through this process:
- Responsive wouldn't be possible because walls are coordinate based.
- There is probably an algorithm I could use to find shortest distance for each crew member.
- Wouldn't it be impressive if I built an optimization algorithm for finding the paths each crew member would have to take to travel the least distance combined?
- Crew members must be varying sizes? Surely some would take up more space than others when going through a door. The path that is drawn should probably reflect this.
- A wall does not have doors. Walls create the illusion of doors (*so deep*).
- I want this to be a directive I can pass different data sets into.

I decided there are 4 main layers to this SVG I was about to build:

1. Crew
2. Walls
3. Ships
4. Paths each crew member should take

#### Limitations

I realized how elaborate this solution could get when I started thinking about walls which existed on multiple planes or were even curved.

Once a wall has as a curve you're talking about finding shortest paths using high order polynomials likely only searchable by some regression modeling.

I decided to stick to walls on one plane, no curves.

Random thoughts:

- Trémaux's algorithm might work if horizontal walls were brought in.
- Dijkstra's algorithm is a go-to for shortest paths.

#### Data structures

I modeled data structures before even writing code. I knew vaguely what all the properties of each "thing" would be. It was important that the directive I was about to build was reusable in different contexts - so I felt like any properties an object could have should also be configurable.

Dynamic configurations across multiple object properties meant a lot of calculations on the fly. I knew this upfront, didn't care, wanted a challenge.

The most interesting data structures are walls.

Walls, as I point out earlier, could exist on multiple planes and have curves. In SVG, there is a convention for drawing curved lines using a `path` object. Although this feature is not reflected in the implementation, there is support for going crazy. The algorithms which detect "doors" or gaps formed by non touching walls would need to be re-written to support something like this.

#### Features I dropped

The full rendering of a spaceship. I didn't feel like this provided much value to the exercise, I felt like circles were enough.

The node counter inside each spaceship. My implementation kind of deviated away from the need for this. For example there may be two spaceships available to the same race, in which case I would render two paths the crew member could take. Sometimes this would lead to double counting.

#### Features I added

Dynamic map, crew, wall, ship generation via JSON input/pseudo-random data generator.

Configurable map size, ship/crew sizes/offsets, wall width, path stroke/offset, door offsets.

Recursive shortest path generator.

Error correction for competing paths.

#### Algorithms leading to paths

Rendering the SVG's for the crew, walls, and ships was pretty straight forward. The paths were a challenge.

My overall strategy for generating paths would be to turn the whole thing into a directed graph. The crew would be top level nodes and the ships would be the bottom.

Although a crew member could take several paths to get to his destination, I felt like I should optimize this, since you know, they are trying to escape.

Since doors weren't explicitly built as a data structure, they had to be detected. I did this because as soon as you introduce doors on multiple planes drawing walls becomes impossible to infer. I concluded that a symphony of walls create the illusion of doors. Two walls that do not touch create a gap - whether you put a block of wood in between and bind some hinges is up to you.

Then I needed to sort all of the walls by their x-planes. In order to build a directed graph, I needed a clear order an algorithm could follow to build an object of relationships. I used another algorithm to build the graph from their sorted x-planes that could now find the distance between neighboring nodes using the Pythagorean Theorem.

Using that graph I could run Dijkstra's algorithm to find some initial shortest paths. Of course, the work isn't over because even though a path may be optimal it may need to be adjusted or removed if a competing path does not allow it to fit through a door!

I used a recursive algorithm to find the next optimal path until all paths were exhausted in which case that individual is not making it off the ship! With random data you will often see many crew members left behind!

After finding stable paths I generated `(x,y)` coordinates for each node. Wall nodes were an exception where two path coordinated were generated - this was to account for the wall thickness so that a member may "pass through".

#### Conclusion

Really cool challenge. I enjoyed this one a lot. As soon as I got to the paths I took a deep breath because I quickly realized how many subtle challenges were ahead of me.

Performance-wise I could definitely make some improvements. I could remove features like "optimal paths" or all the dynamic configurations. I could also rewrite/optimize some of the data structures for better memory use or cut down on iterations.

I added in a pseudo-random data generator so I could show off the configuration. Some really interesting visualizations come out of this. I've saw an edge case where lines were going "underneath" the SVG area - but this comes from the randomly generated wall lengths, which hopefully wouldn't be a part of their original ship design.