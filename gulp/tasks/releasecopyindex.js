var gulp = require('gulp');
var config = require('../config').releasecopyindex;

gulp.task('releasecopyindex', ['releasecleanindex', 'releasejsheader', 'releasecssheader'], function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
