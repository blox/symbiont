var gulp = require('gulp');
var clean = require('gulp-clean');
var config = require('../config').cleanindex;

gulp.task('cleanindex', function () {
  return gulp.src(config.src)
    .pipe(clean());
});
