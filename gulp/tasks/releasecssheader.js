var gulp = require('gulp');
var header = require('gulp-header');
var config = require('../config').releasecssheader;
var pkg = require('../../package.json');
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');
  
gulp.task('releasecssheader', ['releasecss'], function() {
  return gulp.src(config.src)
    .pipe(header(banner, { pkg : pkg }))
    .pipe(gulp.dest(config.dest));
});
