var gulp = require('gulp');
var templates = require('gulp-angular-templatecache');
var config = require('../config').templates;

gulp.task('templates', function () {
  gulp.src(config.src)
    .pipe(templates(config.filename, config.options))
    .pipe(gulp.dest(config.dest));
});