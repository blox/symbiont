var gulp = require('gulp');
var flatten = require('gulp-flatten');
var config = require('../config').copyvendorfonts;

gulp.task('copyvendorfonts', function() {
  return gulp.src(config.src)
    .pipe(flatten())
    .pipe(gulp.dest(config.dest));
});
