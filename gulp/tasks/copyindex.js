var gulp = require('gulp');
var config = require('../config').copyindex;

gulp.task('copyindex', ['cleanindex'], function() {
  return gulp.src(config.src)
    .pipe(gulp.dest(config.dest));
});
